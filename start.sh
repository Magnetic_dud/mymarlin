#!/bin/bash
function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}

function committa {
	echo -n "Ok, type a commit message: ";
	read;
	echo You typed ${REPLY}
	git add -A
	git commit -m "${REPLY}"
	git push
}


# Ansi color code variables
red="\e[0;91m"
blue="\e[0;94m"
expand_bg="\e[K"
blue_bg="\e[0;104m${expand_bg}"
red_bg="\e[0;101m${expand_bg}"
green_bg="\e[0;102m${expand_bg}"
green="\e[0;92m"
white="\e[0;97m"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"

rm -r Marlin-bugfix-2.0.x/
wget https://github.com/MarlinFirmware/Marlin/archive/bugfix-2.0.x.zip
unzip bugfix-2.0.x.zip
rm bugfix-2.0.x.zip
#Configurations-bugfix-2.0.x
wget https://github.com/MarlinFirmware/Configurations/archive/bugfix-2.0.x.zip
unzip bugfix-2.0.x.zip
rm bugfix-2.0.x.zip
rm Marlin-bugfix-2.0.x/Marlin/Configuration.h
rm Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h

#custom changes
mv Configurations-bugfix-2.0.x/config/examples/Creality/Ender-3/CrealityV1/*.h Marlin-bugfix-2.0.x/Marlin/
rm -r Configurations-bugfix-2.0.x/
rm Marlin-bugfix-2.0.x/Marlin/_Statusscreen.h
cp _Statusscreen.h Marlin-bugfix-2.0.x/Marlin/

lineNum="$(grep -n "CUSTOM_MACHINE_NAME" Marlin-bugfix-2.0.x/Marlin/Configuration.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h
start="sed -i '"
middle="s/.*/"
testo='#define CUSTOM_MACHINE_NAME "PrintEasy"'
ending="/' Marlin-bugfix-2.0.x/Marlin/Configuration.h"
#yes_or_no "Change?" && sed -i "${lineNum}'/.*/#define CUSTOM_MACHINE_NAME "PrintEasy"/' Marlin-bugfix-2.0.x/Marlin/Configuration.h
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h

testo="#define LEVEL_BED_CORNERS"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h

testo="#define EEPROM_CHITCHAT"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h
testo="\/\/#define EEPROM_CHITCHAT"
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h

testo="#define LCD_LANGUAGE en"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h
testo="#define LCD_LANGUAGE it"
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h

testo="#define NOZZLE_PARK_FEATURE"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h

testo="#define PID_EDIT_MENU"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h
testo="\/\/#define PID_EDIT_MENU"
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration.h

#edits in Configuration_adv.h
ending="/' Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h"

testo="#define ADVANCED_PAUSE_FEATURE"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h

testo="#define CUSTOM_USER_MENUS"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h

testo="#define USER_DESC_4"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h
testo='#define USER_DESC_4 \"Disabilita endstops\"'
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h

testo="#define USER_GCODE_4"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h
testo='#define USER_GCODE_4 \"M211 S0\"'
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h

testo="#define USER_DESC_3"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h
testo='#define USER_DESC_3 \"Abilita endstops\"'
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h

testo="#define USER_GCODE_3"
lineNum="$(grep -n "$testo" Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h | head -n 1 | cut -d: -f1)"
echo -e "${green_bg}Line to change:${reset}"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h
testo='#define USER_GCODE_3 \"M211 S1\"'
linea=$start$lineNum$middle$testo$ending
eval $linea
echo "Wrote:"
sed -n ${lineNum}p Marlin-bugfix-2.0.x/Marlin/Configuration_adv.h

yes_or_no "Done, commit?" && committa
