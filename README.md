# MyMarlin

This is a bash script that downloads the latest version of marlin from github, then downloads the latest configs, then applies my edits to the firmware, then commits the latest edits. In this way I can have the latest and greatest marlin without messing too much with the source.

How it should work:

1. Download latest Marlin 2.0 bugfix from github

2. Download latest Marlin 2.0 configs from github

3. Unzip both

4. Copy and replace the right configs that I need

5. Search strings to replace

6. row by row show content and "you want to replace with this?"

7. ask for a comment and commit the newly edited source to gitlab (this repository) so i can pull and compile easily on my desktop

**Bugs:** probably many, and I used _many ugly hacks_. You can [open an issue](https://gitlab.com/Magnetic_dud/mymarlin/-/issues) or push a fix :)